﻿const express = require('express');
const bodyParser = require('body-parser');


const OK = 200;
const CREATED = 201;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const SERVER_ERROR = 500;
const NO_CONTENT = 204;
const SEE_OTHER = 303;


function serve(port, model) {
    const app = express();
    app.locals.model = model;
    app.locals.port = port;
    setupRoutes(app);
    app.listen(port, function () {
        console.log(`listening on port ${port}`);
    });
}


function setupRoutes(app) {
    app.use('/users/:id', bodyParser.json());
    app.get('/users/:id', getUser(app)); 
    app.put('/users/:id', newUser(app));  //not REST but illustrates PUT
    app.delete('/users/:id', deleteUser(app)); 
    app.post('/users/:id', updateUser(app));
}

function requestUrl(req) {
    const port = req.app.locals.port;
    return `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
}

module.exports = {
    serve: serve
}

function getUser(app) {
    return function (request, response) {
        const id = request.params.id;
        if (typeof id === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.getUser(id).
                then((results) => response.json(results)).
                catch((err) => {
                    console.error(err);
                    response.sendStatus(NOT_FOUND);
                });
        }
    };
}



function deleteUser(app) {
    return function (request, response) {
        const id = request.params.id;
        if (typeof id === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.deleteUser(id).
                then(() => response.end()).
                catch((err) => {
                    console.error(err);
                    response.sendStatus(NOT_FOUND);
                });
        }
    };
}

function newUser(app) {
    return function (request, response) {
        const id = request.params.id;
        const user = request.body;


        if (typeof id === 'undefined' ||
            typeof user === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        } else if (user.hasOwnProperty("id") && id !== user.id) {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.newUser(id, user).
            then(function (result) {
                response.append('Location', requestUrl(request));
                if (result == "inserted")
                    response.sendStatus(CREATED);
                else if (result == "updated")
                    response.sendStatus(NO_CONTENT);
            }).
            catch((err) => {
                console.error(err);
                response.sendStatus(SERVER_ERROR);
            });
        }
    };
}

function updateUser(app) {
    return function (request, response) {
        const id = request.params.id;
        const user = request.body;

        

        if (typeof id === 'undefined' ||
            typeof user === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        } else if (user.hasOwnProperty("id") && id !== user.id){
                response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.updateUser(id, user).
                then(function (result) {
                    response.append('Location', requestUrl(request));
                    if (result == "update")
                        response.sendStatus(SEE_OTHER);
                }).
                catch((err) => {
                    console.error(err);
                    response.sendStatus(NOT_FOUND);
                });
        }
    };
}


