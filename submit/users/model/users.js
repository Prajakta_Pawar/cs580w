const assert = require('assert');
const ObjectID = require('mongodb').ObjectID;

const USERS = 'users';

const DEFAULT_USERS = './user';
const DEFAULT_INDEXES = { name: 'text', city: 'text' };


function Users(db) {
  this.db = db;
  this.users = db.collection(USERS);
}

Users.prototype.getUser = function(id) {
    const searchSpec = { id: id };
  return this.users.find(searchSpec).toArray().
    then(function(users) {
      return new Promise(function(resolve, reject) {
	if (users.length === 1) {
	  resolve(users[0]);
	}
	else {
	  reject(new Error(`cannot find user ${id}`));
	}
      });
    });
}





Users.prototype.newUser = function (id, userBody) {
    //db.restaurant.replaceOne(
    //    { "name": "Pizza Rat's Pizzaria" },
    //    { "_id": 4, "name": "Pizza Rat's Pizzaria", "Borough": "Manhattan", "violations": 8 },
    //    { upsert: true }
    //);

    //this.users.replaceOne(
    //    searchSpec,
    //    userBody,
    //    { upsert: true }
    //);
   // const searchSpec = { _id: new ObjectID(id) };
    const searchSpec = { id: userBody.id};
    return this.users.replaceOne(searchSpec,userBody,{upsert: true }).
    then(function (result) {
        return new Promise(function (resolve, reject) {
            if (result.matchedCount !== 0) {
                //update
                if (result.modifiedCount !== 1){
                    reject(new Error(`updated ${result.modifiedCount} users`));
                } else {
                    resolve("updated");
                }
            }else {
                //insert
                    if (result.modifiedCount !== 0) {
                        reject(Error(`insert count ${result.insertedCount} !== ` +
                            `${result.length}`));
                    } else {
                        resolve("inserted");
                    }
                }
                
                
                
                    
                  //  reject(new Error(`cannot find user ${id}`));
                
            });
        });
    
}

Users.prototype.updateUser = function (id, userBody) {
    //db.restaurant.replaceOne(
    //    { "name": "Pizza Rat's Pizzaria" },
    //    { "_id": 4, "name": "Pizza Rat's Pizzaria", "Borough": "Manhattan", "violations": 8 },
    //    { upsert: true }
    //);

    //this.users.replaceOne(
    //    searchSpec,
    //    userBody,
    //    { upsert: true }
    //);
    // const searchSpec = { _id: new ObjectID(id) };
    const searchSpec = { id: userBody.id };
    return this.users.replaceOne(searchSpec, userBody).
        then(function (result) {
            return new Promise(function (resolve, reject) {
                
                    if (result.modifiedCount !== 1) {
                        reject(new Error(`updated ${result.modifiedCount} users`));
                    } else {
                        resolve("update");
                    }
                
                
                //  reject(new Error(`cannot find user ${id}`));

            });
        });

}

Users.prototype.deleteUser = function(id) {
    return this.users.deleteOne({ id : id }).
    then(function(results) {
      return new Promise(function(resolve, reject) {
	if (results.deletedCount === 1) {
	  resolve();
	}
	else {
	  reject(new Error(`cannot delete user ${id}`));
	}
      });
    });
}


module.exports = {
    Users: Users
};
