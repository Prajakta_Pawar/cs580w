#!/usr/bin/env nodejs

'use strict';


const process = require('process');

const DB_URL = 'mongodb://localhost:27017/users';

const mongo = require('mongodb').MongoClient;

const options = require('./options').options;

//const options = {
//    port: 1122,
//    sslDir: './'
//}
const server = require('./server/server');
const model = require('./model/model');



const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const crypto = require('crypto-js');
const path = require('path');

const assert = require('assert');
const fs = require('fs');
const https = require('https');
const cookieParser = require('cookie-parser');
const mustache = require('mustache');

//const ws = require('./model/webService');

//const loginJSON = require('D:/Fall 2017/Web/Project 4/webauth/webauth/loginJSON.json');
//const registerJSON = require('D:/Fall 2017/Web/Project 4/webauth/webauth/registerJSON.json');
//const accountJSON = require('D:/Fall 2017/Web/Project 4/webauth/webauth/accountJSON.json');


const loginJSON = require('./loginJSON.json');
const registerJSON = require('./registerJSON.json');
const accountJSON = require('./accountJSON.json');


const CREATED = 201;
const SEE_OTHER = 303;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const UNAUTHORIZED = 401;
const SERVER_ERROR = 500;

const STATIC_DIR = './statics';
const TEMPLATES_DIR = './templates';
const USER_ID = 'usersId'


/*************************** Route Handlers ****************************/

function setupRoutes(app) {
    app.use('/', bodyParser.json());
    app.get('/', loginPageRedirectHandler(app));
    app.get('/register', showRegisterPage(app));
    app.get('/login', showLoginPage(app));

    app.post('/register/submit', registerUserHandler(app));
    app.post('/login/submit', loginUserHandler(app));

    app.get('/account/:id', showAccountPage(app));
    app.get('/logout', logoutUser(app));

    //app.put('/users/:id', registerUser(app));
    //app.get('/users/:id', getUserHandler(app));
    //app.put('/users/:id/auth', loginUserHandler(app));

}

function redirectToRegisterPage(req, res) {
    const port = req.app.locals.port;
    const url = `${req.protocol}://${req.hostname}:${port}/register`;
    res.redirect(url);
}

function redirectToLoginPage(req, res) {
    const port = req.app.locals.port;
    const url = `${req.protocol}://${req.hostname}:${port}/login`;
    res.redirect(url);
}

function loginPageRedirectHandler(app) {
    return function (req, res) {
        res.redirect('login');
    }
}

function logoutUser(app) {
    return function (req, res) {
        res.clearCookie(USER_ID);
        res.redirect('login');
    }
}

function showRegisterPage(app) {
    return function (req, res) {
        res.send(doMustache(app, 'register', registerJSON));
    }
}

function showLoginPage(app) {
    return function (req, res) {
        res.send(doMustache(app, 'login', loginJSON));
    }
}

function loginUserHandler(app) {

    return function (req, res) {
        if (validateLoginParams(req, res)) {
            const userInfo = req.body;
            const token = app.locals.token;
            const email = req.body.emailID
            req.app.locals.model.users.authUser(email, { "pw": userInfo.password }).
                then(function (checkIfValid) {
                    if (checkIfValid.status == 'OK') {
                        app.locals.token = checkIfValid.authToken
                        res.redirect(`/account/${email}`);
                    }
                    else if (checkIfValid == UNAUTHORIZED) {
                        loginJSON.qErrors.push({ qError: "Authorization Failed" })
                        redirectToLoginPage(req, res)
                    } else if (checkIfValid == NOT_FOUND) {
                        loginJSON.qErrors.push({ qError: "Email not found" })
                        redirectToLoginPage(req, res)
                    }
                })
                .catch((err) => {
                    loginJSON.qErrors.push({ qError: "Authorization Failed" })
                    redirectToLoginPage(req, res)
                });
        } else {
            redirectToLoginPage(req, res)
        }
    };
}
function registerUserHandler(app) {
    return function (req, res) {
        if (validateRegistrationParams(req, res)) {
            //const id = hashPassword(req.body.emailID + req.body.password);
            //console.log("validated");
            const userInfo = {
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                emailID: req.body.emailID,
                password: req.body.password,
            }
            req.app.locals.model.users.newUser(userInfo.emailID, userInfo).
                then(function (data) {

                    if (data.status == 'CREATED') {
                        app.locals.token = data.authToken;
                        res.redirect(`/account/${userInfo.emailID}`);
                   }
                    else if (data == 303) {
                        registerJSON.qErrors.push({ qError: "User with this email id already exists." });
                        redirectToRegisterPage(req, res);
                   }else {
                        registerJSON.qErrors.push({ qError: "Server Error" });
                        redirectToRegisterPage(req, res);
                    }
                });
        } else {
            redirectToRegisterPage(req, res);
        }
    };
}

function validateRegistrationParams(req, res) {
    registerJSON.qErrors = []
    const userInfo = req.body;
    const emailID = userInfo.emailID;
    const password = userInfo.password;
    const Cpassword = userInfo.Cpassword;
    let isOk = true;
    if (typeof userInfo === 'undefined') {
        console.error(`missing body`);
        res.status(BAD_REQUEST)
            .json({
                status: 'missing-body',
                info: 'register request must have a body'
            });
        isOk = false;
    }
    else {
        
        if (!validateEmail(emailID)) {
            registerJSON.qErrors.push({ qError: "Please enter valid email id" })
            isOk = false;
        }

        if (validatePassword(password)) {
            if (password !== Cpassword) {
                registerJSON.qErrors.push({ qError: "The value for the password confirmation field must match the password field." })
                isOk = false;
            }
        } else {
            registerJSON.qErrors.push({ qError: "The password should consist of at least 8 characters none of which is a whitespace character and at least one of which is a digit." })
            isOk = false;
        }

        registerJSON.input[0].value = userInfo.firstname;
        registerJSON.input[1].value = userInfo.lastname;
        registerJSON.input[2].value = userInfo.emailID;

    }
    return isOk;
}
function validateEmail(emailID) {
    var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
    return regex.test(emailID);
    
}
function showAccountPage(app) {
    return function (req, res) {
        const id = req.params.id;
        const token = app.locals.token;
        const userCookie = req.cookies[USER_ID];
        if (userCookie === undefined) {
            app.locals.model.users.getUser(id, token)
                .then(function (userInfo) {
                    res.cookie(USER_ID, userInfo, { maxAge: 86400 * 1000 });
                    //console.log(userInfo.firstname)
                    accountJSON.data[0].value = userInfo.firstname;
                    accountJSON.data[1].value = userInfo.lastname;

                    res.send(doMustache(app, 'account', accountJSON));
                }).
                catch((err) => {
                    res.clearCookie(USER_ID);
                    loginJSON.qErrors.push({ qError: "Invalid username or password" })
                    redirectToLoginPage(req, res)
                })
        }
        else {
            //console.log(userCookie.firstname)
            accountJSON.data[0].value = userCookie.firstname;
            accountJSON.data[1].value = userCookie.lastname;
            res.send(doMustache(app, 'account', accountJSON));
        }
        

    };

}
function validatePassword(password) {
    //var regex = /(?=.*?[A-Za-z])(?=.*?[0-9]).{8,}$/;
    var regex = /(?=.*?[0-9]).{8,}$/;
    //var regex = /(?=.*?[0-9])[^\s\\].{8,}$/;
    return regex.test(password);
    //return true;
}
function validateLoginParams(req, res) {
    loginJSON.qErrors = []
    const userInfo = req.body;
    const emailID = userInfo.emailID;
    const password = userInfo.password;
    let isOk = true;
    if (typeof userInfo === 'undefined') {
        console.error(`missing body`);
        res.status(BAD_REQUEST)
            .json({
                status: 'missing-body',
                info: 'login request must have a body'
            });
        isOk = false;
    }
    else if (typeof emailID === 'undefined' || emailID.trim().length === 0) {
        loginJSON.qErrors.push({ qError: "Please enter email id" })
        isOk = false;
    } else if (typeof password === 'undefined' || password.trim().length === 0) {
        loginJSON.qErrors.push({ qError: "Please enter password" })
        isOk = false;
    }
    else {
        
        if (!validateEmail(emailID)) {
            loginJSON.qErrors.push({ qError: "Please enter valid email id" })
            isOk = false;
        }

        loginJSON.input[0].value = userInfo.emailID;
    }
    return isOk;
}
/************************ Utility functions ****************************/

function doMustache(app, templateId, view) {
    let templates = {}
    
    let tempName = templateId;

    if (templateId === 'register') {
        tempName = "format"
        templates = { footer: app.templates.loginFooter };
    } else if (templateId === 'login') {
        tempName = "format"
        templates = { footer: app.templates.registerFooter };
    }
    return mustache.render(app.templates[tempName], view, templates);
}

function errorPage(app, errors, res) {
    if (!Array.isArray(errors)) errors = [errors];
    const html = doMustache(app, 'errors', { errors: errors });
    res.send(html);
}

/*********************** Authentication Token **************************/

const SECRET = '!@1aSd*$aANE#'

/*************************** Password Routines *************************/

const ROUNDS = 10;
function hashPassword(password) {
    return bcrypt.hashSync(password, ROUNDS);
}

function checkPassword(password, hash) {
    return bcrypt.compareSync(password, hash);
}

/************************* Utility Functions ***************************/



function getBearerAuth(req) {
    const authorization = req.get('authorization');
    const m = authorization && authorization.match(/^\s*bearer\s*(\S+)/i);
    return m && m[1];
}



function requestUrl(req) {
    const port = req.app.locals.port;
    const url = `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
    const qIndex = url.indexOf('?');
    return (qIndex < 0) ? url : url.substr(0, qIndex);
}
/*************************** Initialization ****************************/

function setupTemplates(app) {
    app.templates = {};
    for (let fname of fs.readdirSync(TEMPLATES_DIR)) {
        const m = fname.match(/^([\w\-]+)\.ms$/);
        if (!m) continue;
        try {
            app.templates[m[1]] =
                String(fs.readFileSync(`${TEMPLATES_DIR}/${fname}`));
        }
        catch (e) {
            console.error(`cannot read ${fname}: ${e}`);
            process.exit(1);
        }
    }
}

function setup() {
    process.chdir(__dirname);
    const port = options.port;
    const app = express();
    const model1 = new model.Model(options.ws_url);
    app.locals.model = model1;
    app.locals.port = port;
    app.use(cookieParser());
    setupTemplates(app);
    app.use(express.static(STATIC_DIR));
    app.use(bodyParser.urlencoded({ extended: true }));
    setupRoutes(app);
    https.createServer({
        key: fs.readFileSync(`${options.sslDir}/key.pem`),
        cert: fs.readFileSync(`${options.sslDir}/cert.pem`),
    }, app).listen(port, function () {
        console.log(`listening on port ${port}`);
    });
    //app.listen(port, function () {
    //    console.log(`listening on port ${port}`);
    //});
}

setup();
