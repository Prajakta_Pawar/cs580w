'use strict';

const axios = require('axios');


//const WS_URL = 'https://localhost:1234';


function Users(URL) {
    //this.baseUrl = WS_URL;
    this.baseUrl = URL;
}

//All action functions return promises.


Users.prototype.newUser = function (id, userInfo) {
    //console.log(userInfo);
    console.log(`${this.baseUrl}/users/${id}?pw=${userInfo.password}`);
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    return axios.put(`${this.baseUrl}/users/${id}?pw=${userInfo.password}`, userInfo, { maxRedirects: 0 })
        .then((response) => {
            return response.data;
        }).catch((err) => {
            return err.response.status;
        });
}

Users.prototype.getUser = function (id,token) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    return axios.get(`${this.baseUrl}/users/${id}`, {"headers" : {"authorization" : "Bearer " + token}})
        .then((response) => {
            //console.log(response.data)
            return response.data;
        }).catch((err) => {
            return err.response.status;
        });
}


Users.prototype.authUser = function (id, userInfo) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    return axios.put(`${this.baseUrl}/users/${id}/auth`, userInfo)
        .then((response) => {
            return response.data;
        }).catch((err) => {
            return err.response.status;
        });
}
module.exports = {Users: Users}
