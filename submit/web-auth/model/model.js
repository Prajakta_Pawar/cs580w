const users = require('./users');


function Model(URL) {
    this.users = new users.Users(URL);
}


module.exports = {
  Model: Model
};
