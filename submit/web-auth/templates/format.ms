<!DOCTYPE html>
<html>
  <head>
    <title>{{title}}</title>
    <link rel="stylesheet" href="/css/style.css">
  </head>
  <body>
    <h1>{{title}}</h1>
    <p class="error">{{msg}}</p>
	{{#qErrors}}<span class="error">{{qError}}<br></span>{{/qErrors}}
    <form method="POST" action="{{form_action}}">
     <div>
	 {{#input}}
	         <p>
			  
			   <label><span class="label">{{field}}</span></label>
			   <input type="{{type}}" placeholder="Enter {{field}}" name= "{{name}}" value="{{value}}" required />
			 
			 </p>
	 {{/input}}
	 </div>
      <input name="submit" type="submit" value="{{submit_action}}" class="control">
    </form> 
    {{>footer}}
  </body>
</html>