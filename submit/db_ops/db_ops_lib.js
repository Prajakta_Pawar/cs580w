'use strict';

const assert = require('assert');
const mongo = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

//used to build a mapper function for the update op.  Returns a
//function F = arg => body.  Subsequently, the invocation,
//F.call(null, value) can be used to map value to its updated value.
function newMapper(arg, body) {
  return new (Function.prototype.bind.call(Function, Function, arg, body));
}

//print msg on stderr and exit.
function error(msg) {
  console.error(msg);
  process.exit(1);
}

//export error() so that it can be used externally.
module.exports.error = error;


//auxiliary functions; break up your code into small functions with
//well-defined responsibilities.

//perform op on mongo db specified by url.
function dbOp(url, op) {
  //your code goes here
  // var obj = require(op);
//	    console.log("file is : " + obj);
	let obj = JSON.parse(op);
	

	    if (obj.op === "create") {
		            mongo.connect(url, function (err, db) {
				                assert.equal(null, err);
				                insertDocument(db, obj, function () {
							                db.close();
							            });
				            });
		        }else if (obj.op === "read") {
				        mongo.connect(url, function (err, db) {
						            assert.equal(null, err);
						            readDocument(db, obj, function () {
								                    db.close();
								                });
						        });

				    }else if (obj.op === "delete"){
				        mongo.connect(url, function (err, db) {
						            assert.equal(null, err);
						            removeDocument(db, obj, function () {
								                    db.close();
								                });
						        });
					        } else if (obj.op === "update") {
					            mongo.connect(url, function (err, db) {
							                assert.equal(null, err);
							                updateDocument(db, obj, function () {
										                db.close();
										            });
							            });

							    }
}

var insertDocument = function (db, jsonP, callback) {
	    db.collection(jsonP.collection).insertMany(jsonP.args, function (err, result) {
		                assert.equal(err, null);
		               // console.log("Inserted a document into the test collection.");
		                callback();
		            });
};

var readDocument = function (db, jsonP, callback) {
	    var result = db.collection(jsonP.collection).find(jsonP.args);

	    //console.log("Reading a document from test collection.");

	    result.each(function (err, doc) {
		            if (doc !== null) {
				                assert.equal(err, null);
				                console.log(doc);
				                callback();
				            }
	    });
};

var removeDocument = function (db, jsonP, callback) {
	    db.collection(jsonP.collection).remove(jsonP.args, function (err, result) {
		            assert.equal(err, null);
		           // console.log("Deleted a document into the test collection.");
		            callback();
		        });
};

var updateDocument = function (db, jsonP, callback) {
	    var fn_1 = jsonP.fn[0];
	    var fn_2 = jsonP.fn[1];
	   // console.log(fn_1);
	   // console.log(fn_2);

	    var mapper = newMapper(fn_1, fn_2);
	    var entries = jsonP.args;

	if (entries.length !== undefined) {


		       db.collection(jsonP.collection).find(entries).toArray().then(function (result) {
			                 // console.log('result' + result);
			                  for (var i = 0; i < result.length; i++) {
						                 db.collection(jsonP.collection).update(result[i], mapper.call(null, result[i]), function (err, res) {
									                    if (err) throw err;
									                    // console.log(res);
									                    // console.log("doc updated");
									                                        callback();
									                                                       });
									                                                                  }
									                                                                         });
									 
	}else {

			       db.collection(jsonP.collection).find({}, { _id: 0 }).toArray().then(function (result) {
				                 // console.log('result else ' + result);
				                  for (var i = 0; i < result.length; i++) {
							                 db.collection(jsonP.collection).update(result[i], mapper.call(null, result[i]), function (err, res) {
										                    if (err) throw err;
										                    // console.log(res);
										                    // console.log("else doc updated");
										                                        callback();
										                                                       });
										                                                                  }
										                                                                         });}                                                                         
									 
}

//make main dbOp() function available externally
module.exports.dbOp = dbOp;

