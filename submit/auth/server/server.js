const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const https = require('https');
const path = require('path');
var hash = require('bcrypt');
var randomstring = require("randomstring");

const OK = 200;
const CREATED = 201;
const NO_CONTENT = 204;
const MOVED_PERMANENTLY = 301;
const FOUND = 302;
const SEE_OTHER = 303;
const NOT_MODIFIED = 303;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const CONFLICT = 409;
const SERVER_ERROR = 500;
const ERROR_UNAUTHORIZED = 401;

function serve(options, model) {
  const app = express();
  app.locals.model = model;
  app.locals.port = options.port || 443;
  app.locals.authTimeout = options.authTimeout;

  const keyPath = path.join(options.sslDir, 'key.pem');
  const certificatePath = path.join(options.sslDir, 'cert.pem');

  setupRoutes(app);

  var key = fs.readFileSync(keyPath);
  var cert = fs.readFileSync(certificatePath);

  var details = {
      key: key,
      cert: cert
  }; 


  https.createServer(details, app).listen(options.port, function () {
      console.log(`listening on port ${options.port}`);
  });
  
}


function setupRoutes(app) {
  app.use('/users/:id', bodyParser.json());
  app.use('/users/:id', cacheUser(app));
  app.put('/users/:id', newUser(app));
  app.get('/users/:id', getUser(app));
  app.put('/users/:id/auth', authenticateUser(app));
}

module.exports = {
  serve: serve
}

function updateUser(app) {
    return function (request, response) {
        const id = request.params.id;
        const userInfo = request.body;
        if (!request.user) {
            console.error(`user ${request.params.id} not found`);
            response.sendStatus(NOT_FOUND);
        }
        else {
            request.app.locals.model.users.updateUser(id, userInfo).
                then(function (id) {
                    response.redirect(SEE_OTHER, requestUrl(request));
                }).
                catch((err) => {
                    console.error(err);
                    response.sendStatus(SERVER_ERROR);
                });
        }
    };
}
function getUser(app) {
    return function (request, response) {
        const authorization = request.get('Authorization');
        const id = request.params.id;
        if (typeof authorization === 'undefined') {
            response.status(ERROR_UNAUTHORIZED).json({
                "status": "ERROR_UNAUTHORIZED",
                "info": `/users/${id}/auth requires a valid 'pw' password query parameter`
            });
        }
        let token = null;
        if (request.headers.authorization && request.headers.authorization.split(' ')[0] === 'Bearer') {
            token = request.headers.authorization.split(' ')[1];
        }
        let timeObj = new Date().getTime();
        let timestamp = Math.round(timeObj / 1000);
        

        //verify token
        request.app.locals.model.users.getUser(request.params.id).
            then(function (results) {
                

                if (results) {
                    if (timestamp - results['_timestamp'] <= app.locals.authTimeout && token === results['_token']) {
                        //token valid
                        response.status(OK).json(results);
                    } else {
                        response.status(ERROR_UNAUTHORIZED).json({
                            "status": "ERROR_UNAUTHORIZED",
                            "info": `/users/${id} requires a bearer authorization header`
                        });
                    }

                   
                }
                else {
                    response.status(NOT_FOUND).json({
                        "status": "ERROR_NOT_FOUND",
                        "info": `user ${id} not found`
                    });
                }

            });
    
  };
}


function newUser(app) {
    return function (request, response) {
        const userInfo = request.body;
        const id = request.params.id;
        const pass = request.query.pw;
        //let myDateString = Date();
        //console.log(myDateString);
        let timeObj = new Date().getTime();
        let timestamp = Math.round(timeObj / 1000);
        


        if (typeof userInfo === 'undefined' || typeof pass === 'undefined') {
            console.error(`missing body`);
            response.sendStatus(BAD_REQUEST);
        }
        else {
            const password = hash.hashSync(request.query.pw, 10);

            if (request.user) {
                response.location(requestUrl(request)).status(SEE_OTHER).json({
                    "status": "EXISTS",
                    "info": `user ${id} already exists`
                });
                
            } else {
                let token = randomstring.generate();
                
                request.app.locals.model.users.newUser(id, userInfo, password, token, timestamp).
                    then(function (user) {
                        response.location(requestUrl(request));
                        response.status(CREATED).json({
                            "status": "CREATED",
                            "authToken": token
                        });
                        
                    }).
                    catch((err) => {
                        console.error(err);
                        response.sendStatus(SERVER_ERROR);
                    });
            }
        }

    }
}

function authenticateUser(app) {
    return function (request, response) {
        const body = request.body;
        const id = request.params.id;
        if (typeof id === 'undefined' || typeof body === 'undefined') {
            console.error(`missing body`);
            response.sendStatus(BAD_REQUEST);
        } else if (!(body.hasOwnProperty('pw'))) {
            response.status(ERROR_UNAUTHORIZED).json({
                    "status": "ERROR_UNAUTHORIZED",
                    "info": `/users/${id}/auth requires a valid 'pw' password query parameter`
                });
        } else {
            request.app.locals.model.users.getUser(id).
                then(function (results) {
                    
                    if (!results) {
                        response.status(NOT_FOUND).json({
                            "status": "ERROR_NOT_FOUND",
                            "info": `user ${id} not found`
                        });
                    }
                    else {
                        if (hash.compareSync(body['pw'], results['_pw'])) {
                            let token = randomstring.generate();
                            response.status(OK).json(
                                {
                                    "status": "OK",
                                    "authToken": token,
                                }
                            );
                        } else {
                            response.status(ERROR_UNAUTHORIZED).json(
                                {
                                    "status": "ERROR_UNAUTHORIZED",
                                    "info": `/users/${id}/auth requires a valid 'pw' password query parameter`
                                }
                            );
                        }
                    }
                });
                }
        
    };
}



function cacheUser(app) {
  return function(request, response, next) {
    const id = request.params.id;
    if (typeof id === 'undefined') {
      response.sendStatus(BAD_REQUEST);
    }
    else {
      request.app.locals.model.users.getUser(id, false).
	then(function(user) {
	  request.user = user;
	  next();
	}).
	catch((err) => {
	  console.error(err);
	  response.sendStatus(SERVER_ERROR);
	});
    }
  }
}

//Should not be necessary but could not get relative URLs to work
//in redirect().
function requestUrl(req) {
  const port = req.app.locals.port;
  return `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
}

