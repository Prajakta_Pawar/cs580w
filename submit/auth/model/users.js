const assert = require('assert');

const USERS = 'users';
const DEFAULT_USERS = './users_data';
const DATA = '_data';

function Users(db) {
  this.db = db;
  this.users = db.collection(USERS);
}


Users.prototype.getUser = function(id, mustFind=true) {
  const searchSpec = { _id: id };
  return this.users.find(searchSpec).toArray().
    then(function(users) {
      return new Promise(function(resolve, reject) {
          if (users.length === 1) {
           
	  resolve(users[0]);
	}
	else if (users.length == 0 && !mustFind) {
	  resolve(null);
	}
	else {
	  reject(new Error(`cannot find user ${id}`));
	}
      });
    });
}

Users.prototype.newUser = function(id, user,pw,token,timestamp) {
    const d = { _id: id, _pw: pw, DATA: user, _token: token, _timestamp: timestamp };
  return this.users.insertOne(d).
    then(function(results) {
      return new Promise((resolve) => resolve(results.insertedId));      
    });
}


module.exports = {
  Users: Users
};
